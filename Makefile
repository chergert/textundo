all: test-undo

PKGS = gobject-2.0

test-undo: test.c gtktexthistory.c gtktexthistoryprivate.h istring.h
	$(CC) -ggdb -o $@ -Wall $(shell pkg-config --cflags --libs $(PKGS)) test.c gtktexthistory.c

clean:
	rm -f test-undo

